//
//  Customer.hpp
//  robot
//
//  Created by Nohemi Gonzalez on 10/17/16.
//  Copyright © 2016 Nohemi Gonzalez Lopez. All rights reserved.
//

#ifndef Customer_hpp
#define Customer_hpp

#include <stdio.h>
#include "std_lib_facilities.h"
#include "View.hpp"


class Customer{
public:
    Customer(string customer_name, string customer_number):name(customer_name), custnumber(customer_number){}
    Customer():name("unknown"),custnumber("unknown"){}
    string to_string();
    string get_customername();
    string get_customernumber();
private:
    string name;
    string custnumber;
};
#endif /* Customer_hpp */
