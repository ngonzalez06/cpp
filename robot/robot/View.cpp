//
//  View.cpp
//  robot
//
//  Created by Nohemi Gonzalez on 10/12/16.
//  Copyright © 2016 Nohemi Gonzalez Lopez. All rights reserved.
//
#ifdef _MSC_VER
#include "stdafx.h"

#include "View.hpp"
#include "Shop.hpp"
#include <string>


void View::show_menu(){
    cout<<"Main Menu "<<'\n'<<"---------\n"<<"1 (C)reate\n"<<"2 (Report)\n"<<"3 (S)ave\n"<<"(Q)uit"<<endl;
}

void View::create_menu(){
cout<<"Create\n"<<"------\n"<<"1.1 (O)der\n"<<"1.2 (C)ustomer\n"<<"1.3 (S)ale Associate\n"<<"1.4 Robot (M)odel\n"<<"1.5 (R)obot Component\n"<<"1.6 (Q)uit to main menu\n";
    
}
void View::report_menu(){
    cout<<"Report\n"<<"------\n"<<"2.1 (O)der\n"<<"2.2 (C)ustomer\n"<<"2.3 (S)ale Associate\n"<<"2.4 Robot (M)odel\n"<<"2.5 Robot (C)omponent\n"<<"2.6 (Q)uit to main menu\n";
}
void View::list_customer(){
    for(int i = 0; i<shop.numberof_customer;i++){
        cout<<i<<")"<<shop.customer_tostring(i)<<endl;
    }
    
}
void View::list_associates(){
    for(int i = 0; i<shop.numberof_associcate;i++){
        cout<<i<<")"<<shop.associate_tostring(i)<<endl;
    }
    
}
void View::list_componenttype(){
    for (int i=0; i<shop.number_ofcompnenetype(); ++i) {
        cout << i << ") " << shop.componenttype_tostring(i) << endl;
    }
}
#endif
