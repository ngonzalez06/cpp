//
//  arm.cpp
//  robot
//
//  Created by Nohemi Gonzalez on 10/18/16.
//  Copyright © 2016 Nohemi Gonzalez Lopez. All rights reserved.
//

#include "arm.hpp"

String Arm::to_string(){
    string rob = "\"" + name + "\"" + modelnum + ", " + price +" ," +component.to_string();
    return rob;
}
ostream& operator<<(ostream& os, arm& p) {
    os << p.to_string();
    return os;
}
