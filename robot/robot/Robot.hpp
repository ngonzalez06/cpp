//
//  Robot.hpp
//  robot
//
//  Created by Nohemi Gonzalez on 10/12/16.
//  Copyright © 2016 Nohemi Gonzalez Lopez. All rights reserved.
//
#include <iostream>
#include <string>
#ifndef Robot_hpp
#define Robot_hpp
#include "Components.hpp"

using namespace std;
class Robot{
public:
    Robot(string robot_name, string robot_model, string robot_price, Components component) : name(robot_name), modelnum(robot_model), price(robot_price){}
    Robot(): name("unknown"), modelnum("unknown"), price("unknown"){}
    string get_robot_name();
    string get_robot_model_num();
    string get_price();
    
private:
    
    string name;
    string modelnum;
    string price;
//    
//    string torso;
//    string head;
//    string arms;
//    string locometer;
    
};

#endif /* Robot_hpp */
