//
//  arm.hpp
//  robot
//
//  Created by Nohemi Gonzalez on 10/18/16.
//  Copyright © 2016 Nohemi Gonzalez Lopez. All rights reserved.
//

#ifndef arm_hpp
#define arm_hpp
#include "shop.hpp"
#include "Components.hpp"
#include "Robot.hpp"

#include <stdio.h>
#include <string>
#include <iostream>
//using namespace std;
class Arm : public Robot{
public:
    Arm(string robot_name, string robot_model, string robot_price, Component component) : Robot(name(robot_name), modelnum(robot_model), price(robot_price){}
                                                                                                
    virtual string to_string();
    friend ostream& operator<<(ostream& os, Robot& p);


private:

};
#endif /* arm_hpp */
