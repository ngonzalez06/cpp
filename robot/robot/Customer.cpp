//
//  Customer.cpp
//  robot
//
//  Created by Nohemi Gonzalez on 10/17/16.
//  Copyright © 2016 Nohemi Gonzalez Lopez. All rights reserved.
//

#include "Customer.hpp"
#include "std_lib_facilities.h"
string Customer::to_string(){return name + " (" + custnumber +" )";}
string Customer::get_customernumber(){return custnumber;}
string Customer::get_customername(){return name;}

