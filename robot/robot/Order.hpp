//
//  Order.hpp
//  robot
//
//  Created by Nohemi Gonzalez on 10/12/16.
//  Copyright © 2016 Nohemi Gonzalez Lopez. All rights reserved.
//

#ifndef Order_hpp
#define Order_hpp

#include <stdio.h>
#include "std_lib_facilities.h"
#include <string>
#include "Customer.hpp"
#include "Associate.hpp"
//#include "Shop.cpp"
//#include "Shop.hpp"
//#include <iostream>
//#include <vector>


class Order{
    public:
    Order( Customer o_customer, string price,  string balance, string o_date, associate a_associate,balance o_bal): customer(o_customer), date(o_date),  balance(o_bal){}
    
    string to_string();
    
    bool has_balance();
    void bal();
    void robot_part();
private:
    string order_number;
    string price;
    string date;
    string payment;
    Associate associcate;
    Customer customer;
    string balance;
    
    
};

#endif /* Order_hpp */
