//
//  Components.cpp
//  robot
//
//  Created by Nohemi Gonzalez on 10/17/16.
//  Copyright © 2016 Nohemi Gonzalez Lopez. All rights reserved.
//

#include "Components.hpp"
//#include "std_lib_facilities.h"

#include <string>
using namespace std;

string Components::to_string(){
    string comp = name + " ," + partnum + " ," + componenttype.to_string() + " ," + weight + " ," + cost + " ," + description;
    return comp;
}
