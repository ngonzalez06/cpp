//
//  main.cpp
//  homework2b
//
//  Created by Nohemi Gonzalez on 9/7/16.
//  Copyright © 2016 Nohemi Gonzalez. All rights reserved.
//

#include <iostream>
#include <vector>
using namespace std;
int i;
//mgp class
class Mpg_log{
private:
    vector<double> last_odo;//vector to hold previous odometer
    vector<double> this_odo;//vector to hod new odometer
    vector<double> this_gas;//gas used
    
public:
    void start(double starting);
    void buy_gas(double odometer, double gas);
    double get_current_mpg(){
        return (this_odo[i]-last_odo[i])/this_gas[i];}//(new_odometer-previous_odometer)/gas
    double get_total_mgp(){//gets the total average mpg since log started
        int count = i;
        double gas_total=0,mpg ;
    
        while(count>=0){//gets total gas added
            gas_total += this_gas[count];
            count--;
        }
        mpg=(this_odo[i]-last_odo[0])/gas_total;//subtract last odometer - first odometer entry/ divede by total gas
        return mpg;
    }
};
void Mpg_log::buy_gas(double odo, double ga){//pushes new odometer and gas
    this_odo.push_back(odo);
    this_gas.push_back(ga);
}
void Mpg_log::start(double starting){//pushes previous odometer
    last_odo.push_back(starting);}

int main(){
    double initial, odo, gas;
    Mpg_log lg;//creates a class named lg
    
    cout<< "Initial odometer:  ";
    cin>>initial;
    lg.start(initial);//send initial to class
    do{//start loop to ask for new odometer and gas
        cout<<"odometer: ";
        cin>>odo;
        cout<<"Gallons: ";
        cin>>gas;
        lg.buy_gas(odo, gas);
        cout<< "this mpg: "<<lg.get_current_mpg()<<endl;//prints this mpg by calling current_mpg function
        cout<< "Total mpg: "<<lg.get_total_mgp()<<endl;// prints total mpg by calling total mpg
        i+=1;//update counter i
        lg.start(odo);//send second input of odometer to update first odometer with it
    }
    while(odo!=0);
    return 0;
    
}
