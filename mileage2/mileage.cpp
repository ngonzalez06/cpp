//
//  main.cpp
//  Homework2
//
//  Created by Nohemi Gonzalez on 9/7/16.
//  Copyright © 2016 Nohemi Gonzalez. All rights reserved.
//

#include <iostream>
#include <vector>
using namespace std;
int i;
//mgp class
class Mpg_log{
private:
    vector<double> last_odo;//vector to hold previous odometer
    vector<double> this_odo;//vector to hod new odometer
    vector<double> this_gas;//gas used
  
public:
    void start(double starting);
    void buy_gas(double odometer, double gas);
    double get_current_mpg(){
        return (this_odo[i]-last_odo[i])/this_gas[i];}//(new_odometer-previous_odometer)/gas
};
void Mpg_log::buy_gas(double odo, double ga){//pushes new odometer and gas
    this_odo.push_back(odo);
    this_gas.push_back(ga);
}
void Mpg_log::start(double starting){//pushes previous odometer
    last_odo.push_back(starting);}

int main(){
    double initial, odo, gas;
    Mpg_log lg;//creates a class named lg
    
    cout<< "Initial odometer:  ";
    cin>>initial;
    lg.start(initial);//send initial to class
    do{//start loop to ask for new odometer and gas
        cout<<"odometer: ";
        cin>>odo;
        cout<<"Gallons: ";
        cin>>gas;
        lg.buy_gas(odo, gas);
        cout<< "this mpg: "<<lg.get_current_mpg()<<endl;
        i+=1;
        lg.start(odo);
    }
    while(odo!=0);
    return 0;
    
}
//class Mpg_log{
//private:
//    double last_odo;
//    double this_odo;
//    double this_gas;
//public:
//    Mpg_log(double starting): last_odo(starting){}
//    void buy_gas(double odometer, double gas);
//    double get_current_mpg(){
//        return (this_odo-last_odo)/this_gas;}
//};
//void Mpg_log::buy_gas(double odo, double ga){
//    this_odo = odo;
//    this_gas = ga;
//}
//int main(){
//    double initial, odo, gas;
//    cout<< "Initial odometer:  ";
//    cin>>initial;
//    Mpg_log foo(initial);
//    while(initial!=0){
//    cout<<"odometer: ";
//    cin>>odo;
//    cout<<"Gallons: ";
//    cin>>gas;
//    foo.buy_gas(odo, gas);
//    cout<< "this mpg:"<<foo.get_current_mpg()<<endl;
//        Mpg_log foo(odo);
//    }
//    return 0;
//    
//}
