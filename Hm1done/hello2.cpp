//
//  main.cpp
//  hello2
//
//  Created by Nohemi Gonzalez on 8/30/16.
//  Copyright © 2016 Nohemi Gonzalez. All rights reserved.
//

#include <iostream>
using namespace std;

#include <unistd.h>

int main()
{
    int len;
    string name;
    name = getenv("USER");
    cout << "Hello, " << name << "!\n";
    return 0;
}
