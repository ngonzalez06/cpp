//
//  main.cpp
//  hello1
//
//  Created by Nohemi Gonzalez on 8/30/16.
//  Copyright © 2016 Nohemi Gonzalez. All rights reserved.
//

#include <iostream>
using namespace std;
int main(int argc, const char * argv[]) {
    string name;
    cout<<"What is your name? ";
    getline(cin,name);
    cout << "Hello, "<<name<<"!"<<endl;
    return 0;
}
