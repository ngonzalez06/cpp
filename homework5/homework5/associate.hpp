//  Associate.hpp
//  robot
//  Created by Nohemi Gonzalez on 10/12/16.
//  Copyright © 2016 Nohemi Gonzalez Lopez. All rights reserved.

#ifndef associate_hpp
#define associate_hpp

#include <iostream>
#include <string>

using namespace std;

class Associate{

    string name;
    public:
    Associate() : name(""){}
    Associate(string Associate_name);
  
    
    string get_assname()const;
    string ass_tosetring(Associate& a)const;
    
 
    
};
#endif /* Associate_hpp */
