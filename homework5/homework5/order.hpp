//
//  Order.hpp
//  robot
//
//  Created by Nohemi Gonzalez on 10/12/16.
//  Copyright © 2016 Nohemi Gonzalez Lopez. All rights reserved.
//

#ifndef order_hpp
#define order_hpp


#include "customer.hpp"
#include "associate.hpp"

#include <string>
#include <iostream>


using namespace std;

class Order{
    string order_number;
    string price;
    string date;
    string payment;
    Associate associate;
    Customer customer;
    string balance;
public:
    Order():customer(Customer()), order_number(""), price(""), balance(""), date(""), associate(Associate()){ }
    Order(Customer o_customer, string o_number, string o_price,  string o_balance, string o_date, Associate a_associate);
    //setters
//    void set_ordernum(string ord);
//    void set_balance(string bal);
//    void set_customer(string cust);
//    void set_payment(string pay);
//    void set_price(string pri);
    
    
    
    
    //accesor
    string get_ordernum()const;
    string get_balance()const;
    string get_customer()const;
    string get_payment(string total, int payment)const;
    string get_price()const;
    string to_string(Order& ord)const;
    string get_date()const;
    
//

    bool has_balance();
    void bal();
    void robot_part();

    
};

#endif /* Order_hpp */
