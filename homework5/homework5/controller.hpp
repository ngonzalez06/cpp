//
//  Controller.hpp
//  robot
//
//  Created by Nohemi Gonzalez on 10/12/16.
//  Copyright © 2016 Nohemi Gonzalez Lopez. All rights reserved.
//

#ifndef controller_hpp
#define controller_hpp

#include <stdio.h>
#include "view.hpp"
using namespace std;

class Controller{
    public:
 //   Controller(Shop& shp) : shop(shp){}

        Controller(Shop& shp) : shop(shp), view(View(shop)){}
    
        void cli();
        void main_cmd(int cmd);
        void create_cmd(int cmd);
        void report_cmd(int cmd);
        void robotoptions();
    private:
        Shop& shop;
        View view;
};
#endif /* Controller_hpp */
