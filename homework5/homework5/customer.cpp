//
//  Customer.cpp
//  robot
//
//  Created by Nohemi Gonzalez on 10/17/16.
//  Copyright © 2016 Nohemi Gonzalez Lopez. All rights reserved.
//

#include "customer.hpp"
#include <string>
using namespace std;

Customer::Customer(string customer_name, string customer_number): name(customer_name), custnumber(customer_number){}
string Customer::getcust(Customer& cust)const{return cust.name+" "+cust.custnumber;}
string Customer::custto_string()const{
    string a= get_customername() + " (" + get_customernumber() +" )";
    return a;}

string Customer::get_customernumber()const{return custnumber;}

string Customer::get_customername()const{return name;}
