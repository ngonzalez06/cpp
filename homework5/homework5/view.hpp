//
//  View.hpp
//  robot
//
//  Created by Nohemi Gonzalez on 10/12/16.
//  Copyright © 2016 Nohemi Gonzalez Lopez. All rights reserved.
//

#ifndef view_hpp
#define view_hpp

#include "shop.hpp"
#include "robot.hpp"

using namespace std;

class View{
    public:
    View(Shop& shp) : shops(shp){}
        void show_menu();
        void create_menu();
        void report_menu();
        void list_associates();
        void list_customer();
        void list_componenttype();
        void robotoptions();
        void createparts();
        void component_list();
    
    private:
        Shop& shops;
};
#endif /* View_hpp */
