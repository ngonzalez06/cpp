//
//  shop.hpp
//  shop
//
//  Created by Nohemi Gonzalez on 10/18/16.
//  Copyright © 2016 Nohemi Gonzalez Lopez. All rights reserved.
//

#ifndef shop_hpp
#define shop_hpp


#include "customer.hpp"
#include "robot.hpp"
#include "associate.hpp"
#include "order.hpp"

#include <vector>
#include <iostream>
#include <string>

using namespace std;

class Shop{
public:
    Shop();
    //setters
    void add_robot(Robot& robot);
    void add_order(Order& ord);
    void add_customer(Customer& cust);
    void add_associate(Associate& ass);
    
    //getters
    void printrob()const;
    void printorde()const;
    void sale();
    void printcustomer()const;
    void printassociate()const;
    
    long int number_ofrobot();
    long int number_ofcustomer();
    long int number_ofassociate();
    long int number_oforder();
    
    //object getter
    Order order_return(int index);
    Associate associate_return(int index);
    Customer customer_return(int index);
    Robot robot_tostring(int index);
    string order_tostring(int index);
    string customer_tostring(int index);
    string associate_tostring(int index);
    //robotparts
    
    //robot vectors
    //robot vectors
    //robot vectors
    //robot vectors
    //setter
    void add_locomoterpower(string power);
    void add_armpower(string power);
    void add_baterries(string addbat);
    void add_torso(string addtoroso);
    void add_head(string addhead);
    void add_arm(string addarm);
    void add_locomotor(string addlocomotor);
    void add_maxbat(int max);
    void add_price(string price);
    //robotparts getter
    
    
    
    void printhead();
    void printbat();
    void printloco();
    void printarm();
    void printtoros();
    
    
    string desp();
    string get_batteries(int index);
    string get_torso(int index);
    string get_locomotorpower( int index);
    string get_armpower(int index);
    string get_head(int index);
    string get_arm(int index);
    string get_locomotor(int index);
    string get_speed();
    string get_getprice(int index);

    
    string choosearm(int a);
    string choosetorso(int a);
    string choosebattery(int a);
    string chooselocomotor(int a);
    string choosehead(int a);
    //robot parts size
    int maxbatnum(int i);
    unsigned int headnum();
    unsigned int torsonum();
    unsigned int armnum();
    unsigned int batterynum();
    unsigned int locomotornum();
    unsigned int armpowsizer();
    
    
    void eggshell();
    
private:
    vector<Robot> robots;
    vector<Order> orders;
    vector<Customer> customers;
    vector<Associate> associates;
    vector<int> maxbat;
    vector<string> batteries;
    vector<string> torso;
    vector<string> head;
    vector<string> arm;
    vector<string> armpower;
    vector<string> locomotorpower;
    vector<string> locomotor;
    vector<string> price;
    vector<string> speed;
    
    
};


#endif /* shop_hpp */
