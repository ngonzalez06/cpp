//
//  shop.cpp
//  shop
//
//  Created by Nohemi Gonzalez on 10/18/16.
//  Copyright © 2016 Nohemi Gonzalez Lopez. All rights reserved.
//

#include "shop.hpp"
#include <iostream>



using namespace std;
Shop::Shop(){}

    void Shop::add_robot(Robot& robot){robots.push_back(robot);}
    void Shop::add_order(Order& ord){orders.push_back(ord);}
    void Shop::add_associate(Associate& ass){associates.push_back(ass);}
    void Shop::add_customer(Customer& cust){customers.push_back(cust);}
    
    long int Shop::number_ofrobot(){int long a= robots.size();
    return a;}
    long int Shop::number_ofcustomer(){ return customers.size();}
    long int Shop::number_ofassociate(){return associates.size();}
    long int Shop::number_oforder(){return orders.size();}

//getters

void Shop::printrob()const{
    int i=0;
    for(auto rob: robots){
        cout<<i<<"Head: "<<rob.Head<<" Torso: "<<rob.Torso<< "Battery(ies): "<<rob.Batteries<<" Locomotor: "<<rob.Locomotor<< " Model: "<< rob.Description<<endl;
        i++;
    }
}

void Shop::printorde()const{
    cout<<"Robot Orders: ";
    int i = 0;
    for(auto ord : orders){
        cout<<ord.to_string(ord)<<endl;
//        cout<<i<<"Customer: "<<ord.get_customer()()<<"Order Number: "<<ord.get_ordernum()<< "Price: "<<ord.get_price() << "Balance:  //"<<ord.get_balance()<<"date: "<<ord.get <<" Associate: " <<ord.associate.get_assname()>>endl;
        i++;
    }

}
void Shop::printcustomer()const{
    int i=1;
    for(Customer a : customers){
        cout<<i<<" "<<a.custto_string();
    }
}

void Shop::printassociate()const{
    int i=1;
    for(Associate a : associates){
        cout<<i<<a.ass_tosetring(a)<<endl;
    }
}
void Shop::printhead(){
    int i = 0;
    for(auto rhead:head){
        cout<<i<<" "<<rhead<<endl;
        i++;
    }
}
void Shop::printbat(){
    int i = 0;
    for(auto rbat: batteries){
        cout<<i<<" "<<rbat<<endl;
        i++;
    }
}
void Shop::printloco(){
    int i = 1;
    for(auto rhead:locomotor){
        cout<<i<<" "<<rhead<<endl;
        i++;
    }
}
void Shop::printarm(){
    int i = 1;
    for(auto rarm : arm){
        cout<<i<<" "<<rarm<<endl;
        i++;
    }
}
void Shop::printtoros(){
    int i = 0;
    for(auto rtorso:torso){
        cout<<i<<"rtorso"<<endl;
        i++;
    }
}





Customer Shop::customer_return(int index){return customers.at(index);
    }
    

Robot Shop::robot_tostring(int index){return robots.at(index);}
//string Shop::order_tostring(int index){return orders[index].to_string();}
string Shop::customer_tostring(int index){return customers[index].custto_string();}
string Shop::associate_tostring(int index){return associates[index].get_assname();}
Associate Shop::associate_return(int index){return associates.at(index);}
Order Shop::order_return(int index){return orders.at(index);}
//robot parts setter and getters bellow
void Shop::add_maxbat(int max){
    maxbat.push_back(max);}
void Shop::add_baterries(string addbat){batteries.push_back(addbat);}
void Shop::add_torso(string addtor){torso.push_back(addtor);}
void Shop::add_head(string addhead){head.push_back(addhead);}
void Shop::add_arm(string addarm){arm.push_back(addarm);}
void Shop::add_locomotor(string addlocomotor){locomotor.push_back(addlocomotor);}
void Shop::add_locomoterpower(string power){locomotorpower.push_back(power);}
void Shop::add_armpower(string power){armpower.push_back(power);}
void Shop::add_price(string pric){price.push_back(pric);}
//return size
unsigned int Shop::headnum(){return head.size();}
unsigned int Shop::torsonum(){return torso.size();}
unsigned int Shop::armnum(){return arm.size();}
unsigned int Shop::batterynum(){return batteries.size();}
unsigned int Shop::locomotornum(){return locomotor.size();}
unsigned int Shop::armpowsizer(){return armpower.size();}

//robot parts getters vectors

string Shop::get_batteries(int index){return batteries[index];}
string Shop::get_torso(int index){return torso[index];}
string Shop::get_head(int index){return head[index];}
string Shop::get_arm(int index){return arm[index];}
string Shop::get_locomotor(int index){return locomotor[index];}
string Shop::get_locomotorpower( int index){return locomotor[index];}
string Shop::get_armpower(int index){return armpower[index];}
string Shop::get_getprice(int index){return price[index];}
//string Shop::get_speed(int index){return speed[index];}
//string Shop::get_getprice(Robot& rop){return Price;}
string Shop::desp(){
    string descript;
    cout<<"enter a description"<<endl;
    cin.ignore();
    cin>>descript;
    
    return descript;
}
string Shop::get_speed(){
    string speed;
    cout<<"enter speed"<<endl;
    cin>>speed;
    cin.ignore();
    return speed;
}

int Shop::maxbatnum(int i){return maxbat.at(i);}

string Shop::choosearm(int a){
    return arm.at(a);
    
}
string Shop::choosetorso(int a){
    
    string max = ::to_string(maxbat.at(a));
    
    string tor=torso.at(a)+", max speed:"+max;
    return torso.at(a);
}
string Shop::choosebattery(int a){
    return batteries.at(a);
}
string Shop::chooselocomotor(int a){
    string name= locomotor.at(a)+"max speed:"+locomotorpower.at(a);
    return name;
    
}
string Shop::choosehead(int a){
    string name = head.at(a);
    return name;
}











void Shop::eggshell()
{
    Customer cust{"Nohemi","111"};
    add_customer(cust);
    Associate Ass{"megan"};
    add_associate(Ass);
    Customer cust2{"Rob","112"};
    add_customer(cust2);
    Associate Ass2{"Steve"};
    add_associate(Ass2);
    
    Robot rob;
    add_arm("orange arm");
    add_arm("pink arm");
    add_arm("white arm");
    add_torso("white torso");
    add_torso("orange torso");
    add_torso("pink torso");
    add_maxbat(1);
    add_maxbat(2);
    add_maxbat(3);
    add_baterries("white");
    add_baterries("orange");
    add_baterries("pink");
    add_price("100");
    add_price("100");
    add_price("100");
    add_armpower("10");
    add_armpower("5");
    add_armpower("15");
    add_locomotor("white locomotor");
    add_locomotor("pink locomotor");
    add_locomotor("orange locomotor");
    add_locomoterpower("15");
    add_locomoterpower("10");
    add_locomoterpower("20");
    add_head("white head");
    add_head("pink head");
    add_head("orange head");
    string bat ="1 "+choosebattery(1);
    Robot r{choosehead(1), choosetorso(1), bat, choosearm(1), chooselocomotor(1), "first model 111","231"};
    bat ="2 "+choosebattery(2);
    add_robot(r);
    Robot r2{choosehead(2), choosetorso(2), bat ,choosearm(2), chooselocomotor(2), "second model 111","200"};
    add_robot(r2);
 //   create order
    Order o{customer_return(1), "ordernum1", get_getprice(1), "100", "10/20/16",associate_return(1)};
    add_order(o);
    
    
}


    
    
    
    
    
    
    
    
    
    
 

