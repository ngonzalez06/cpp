
////
////  main.cpp
////  homework5
////
////  Created by Nohemi Gonzalez on 10/19/16.
////  Copyright © 2016 Nohemi Gonzalez Lopez. All rights reserved.
////
//
//#include <iostream>
//#include "shop.hpp"
//#include "controller.hpp"
//
//
//using namespace std;
//
////int main() {
////    Shop shop;
////    Controller controller(shop);
////    //View view(shop);
////    controller.cli();
////
////    return 0;
////}

#include <FL/Fl_Output.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Window.H>
#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Scroll.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Pixmap.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Group.H>
#include <FL/Fl_Choice.H>
#include <FL/fl_ask.H>
#include <FL/Fl_Tabs.H>
#include <FL/Fl_Menu_Bar.H>
#include <FL/Fl_Box.H>
#include <Fl/Fl_Hold_Browser.H>
#include <FL/Fl_Text_Buffer.H>
#include <FL/Fl_Text_Display.H>
#include <Fl/Fl_Input.H>
#include <string.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iomanip>
#include <vector>

#include <FL/Fl_Return_Button.H>
#include <FL/Fl_Multiline_Input.H>
//
#include <SystemConfiguration/SystemConfiguration.h>


using namespace std;
using std::vector;
//#define int tcount = 1;
#define FONT_SERIF 1
#define FONT_SANS_SERIF 2
#define FONT_MONOSPACE 3
#define TAB_XYWH 10,80,1024-10,768-40    // x,y,w,h size of tab containers

#define JPGFILE "/Users/nohemi/git/cpp/homework5/homework5/Robot_Images/desktp.jpg"
static const char *save_xpm[] = { "16 16 9 1",
    " c None",
    ". c #000000",
    "+ c #0000FF", "@ c #000080", "# c #A0A0A0", "$ c #0000C0", "% c #FFFFFF", "& c #C0C0FF", "* c #DCDCDC", "................", ".+@##########@+.", ".$@%%%%%%%%%%@$.", ".+@%%%%%%%%%%@+.", ".+@%########%@+.", ".+@%%%%%%%%%%@+.", ".+@%########%@+.", ".+@&%%%%%%%%&@+.", ".++@@@@@@@@@@++.", ".++++++++++++++.", ".++$......@@$++.", ".++.******.+@++.", ".++.*@+***.+@++.",
    
    ".++.*@+***.+@++.",
    ".@+.******.+@++.",
    " ..............."};


//
//Declarations (we'll define later just neet to declare variables
//



//class Tabhor *tabh;
void ConstructTabChild1(Fl_Group *tab);
void ConstructTabChild2(Fl_Group *tab);
void tab_verticalBC(Fl_Group *tab);

void ConstructTab(Fl_Group *tab);
void menu_CB_logout(Fl_Widget * w, void*);
void menu_CB_Save(Fl_Widget * w, void*);
void menu_CB_Quit(Fl_Widget * w, void*);
void menu_CB_Help(Fl_Widget * w, void*);
void ConstructTab(Fl_Group *tab);


void SelectGroup1_CB(Fl_Widget*, void *);
void SelectGroup2_CB(Fl_Widget*, void *);

//class Toolbar;
void cb_save(Fl_Widget *w, char *data);
void cb_cust(Fl_Widget *w, void*);
void create_customerCB(Fl_Widget* w, void* p);
void cancel_customerCB(Fl_Widget* w, void* p);
void create_associateCB(Fl_Widget* w, void* p);
void cancel_associateCB(Fl_Widget* w, void* p);
void create_orderCB(Fl_Widget* w, void* p);
void cancel_orderCB(Fl_Widget* w, void* p);
void create_partsCB(Fl_Widget* w, void* p);
void cancel_partsCB(Fl_Widget* w, void* p);

//Shop shop;
//Controller controller(shop);
//View view(shop);
//Customer customer;
class Customer_Dialog;
class Associate_Dialog;
class Order_Dialog;
class Parts_Dialog;
class vholder;
vholder *holder;
Parts_Dialog *p_dlg;
Order_Dialog *o_dlg;
Customer_Dialog *c_dlg;
Associate_Dialog *a_dlg;

//
// Widgets
//

Fl_Window* win;
char * foo;
Fl_Hold_Browser *bro=0;
Fl_Hold_Browser *bro2=0;
Fl_Group *grp1 [5]={0,0,0,0,0};
Fl_Group *grp2 [4]={0,0,0,0};
//
//vector class holder and easteregg
//
class vholder{
public:

vector<string> head;
vector<string> type;
vector<string> weight;
vector<string> custnum;
vector<string> price;
vector<string> description;
vector<string> locomotor;
vector<string> maxspeed;
vector<string> armKilowats;
vector<string> associate;
vector<string> customer;
vector<string> customer_number;
vector<string> order;
vector<string> torso;
vector<string> battery;
vector<string> maxbat;
vector<string> arms;
vector<string> robot;
void phead(string a){
   head.push_back(a);}
void pweight(string a){
   weight.push_back(a);}
void pcustnum(string a){
   custnum.push_back(a);}
void pprice(string a){weight.push_back(a);}
void pdescription(string a){description.push_back(a);}
void plocomotor(string a){locomotor.push_back(a);}
void pmaxspeed(string a){maxspeed.push_back(a);}
void parmkilowats(string a){armKilowats.push_back(a);}
void passociate(string a){associate.push_back(a);}
void pcustomer(string a){customer.push_back(a);}
void porder(string a){order.push_back(a);}
void ptorso(string a){torso.push_back(a);}
void pbattery(string a){battery.push_back(a);}
void pmaxbat(string a){maxbat.push_back(a);}
void parms(string a){arms.push_back(a);}
void probot(string a){robot.push_back(a);}



void eggshell(){
   customer.push_back(("Mary"));
   customer.push_back("John");
   customer_number.push_back("111");
   customer_number.push_back("222");
   associate.push_back("Nohemi");
   associate.push_back("Robert");
   torso.push_back("orange torso");
   torso.push_back("white torso");
   maxbat.push_back("1");
   maxbat.push_back("3");
   battery.push_back("1-orange Battery");
   battery.push_back("3-white Battery");
   head.push_back("orange head");
   head.push_back("white head");
   arms.push_back("orange arms");
   arms.push_back("white arms");
   armKilowats.push_back("2");
   armKilowats.push_back("3");
   locomotor.push_back("orange locomotor");
   locomotor.push_back("white locomotor");
   maxspeed.push_back("20");
   maxspeed.push_back("15");
   robot.push_back(customer[1]+customer_number[1]+associate[1]+torso[2]+battery[1]+head[1]+arms[2]+locomotor[2]);
   order.push_back(customer[1]+associate[1]+robot[1]);
}
};




//
//adding parts dialog
//

class Parts_Dialog{
public:
    Parts_Dialog(){
        //dialog = new Fl_Window(340,270,"Add New Customer");
        grp1[3] = new Fl_Group(172,88,1024-200,768-110,"Robot Part");
        grp1[3]->box(FL_ENGRAVED_BOX);
        grp1[3]->align(FL_ALIGN_INSIDE|FL_ALIGN_TOP);
        grp1[3]->labelsize(24);
        p_torso = new Fl_Input(170+140,160+00,200,20,"Torso:");
        p_batnum = new Fl_Input(170+140,160+30,200,20,"Number of Batteries:");
        p_battery = new Fl_Input(170+140,160+60,200,20,"name of Battery:");
        p_head = new Fl_Input(170+140,160+90,200,20,"head");
        p_torso = new Fl_Input(170+140,160+120,200,20,"torso");
        p_arm = new Fl_Input(170+140,160+150,200,20,"Arm:");
        p_locomotor = new Fl_Input(170+140,160+180,200,20,"Locomotor:");
        p_Maxspeed = new Fl_Input(170+140,160+210,200,20,"Maxspeed:");
        
        
        p_create = new Fl_Return_Button(200,160+240,120,25, "Create");
        p_create->callback((Fl_Callback *)create_orderCB,0);
        p_cancel = new Fl_Return_Button(400,160+240,120,25, "Cancel");
        p_cancel->callback((Fl_Callback *)cancel_orderCB,0);
        grp1[3]->end();
        
        
    }
    void show(){grp1[1]->show();}
    void hide () {grp1[1]->hide();}
    
    string torso() {return p_torso->value();}
    string head() {return p_head->value();}
    string batnum() {return p_batnum->value();}
    string battery() {return p_battery->value();}
    string arm() {return p_arm->value();}
    string locomotor() {return p_locomotor->value();}
   string  maxSpeed() {return p_Maxspeed->value();}
private:
    ;
    Fl_Input *p_torso;
    Fl_Input *p_batnum;
    Fl_Input *p_battery;
    Fl_Input *p_head;
    Fl_Input *p_arm;
    Fl_Input *p_locomotor;
    Fl_Input *p_Maxspeed;
    Fl_Return_Button *p_create;
    Fl_Return_Button *p_cancel;
};

//callback parts Dialog
void menu_create_partsCB(Fl_Widget* w, void* p){
    p_dlg->show();
}

void create_partsCB(Fl_Widget* w, void* p) {
    cout<<"### Creating Customer  : "<<endl;
    cout<<"Torso                  : "<<p_dlg->torso() <<endl;
    cout<<"Head                   : "<<p_dlg->head() <<endl;
    cout<<"battery                : "<<p_dlg->battery() <<endl;
    cout<<"number of batteries    : "<<p_dlg->batnum() <<endl;
    cout<<"Arm                    : "<<p_dlg->arm() <<endl;
    cout<<"Loconotor              : "<<p_dlg->locomotor() <<endl;
    holder->ptorso(p_dlg->torso());
    holder->phead(p_dlg->head());
    holder->pbattery(p_dlg->battery());
    holder->pmaxbat(p_dlg->batnum());
    holder->parms(p_dlg->arm());
    holder->plocomotor(p_dlg->locomotor());

    p_dlg->hide();
}
void cancel_partsCB(Fl_Widget* w, void* p) {
    p_dlg->hide();
}





//
//order dialog
//

class Order_Dialog{
public:
    Order_Dialog(){
    grp1[0] = new Fl_Group(172,88,1024-200,768- 110,"Create Order");
    grp1[0]->box(FL_ENGRAVED_BOX);
    grp1[0]->align(FL_ALIGN_INSIDE|FL_ALIGN_TOP);
    grp1[0]->labelsize(24);
    p_torso = new Fl_Input(170+140,160+00,200,20,"Torso");
    p_batnum = new Fl_Input(170+140,160+30,200,20,"Number of Batteries ");
    p_battery = new Fl_Input(170+140,160+60,200,20,"name of Battery");
    p_head = new Fl_Input(170+140,160+90,200,20,"head");
    p_torso = new Fl_Input(170+140,160+120,200,20,"torso");
    p_arm = new Fl_Input(170+140,160+150,200,20,"Arm:");
    p_locomotor = new Fl_Input(170+140,160+180,200,20,"locomotor");
    p_create = new Fl_Return_Button(200,380,120,25, "Create");
    p_create->callback((Fl_Callback *)create_partsCB,0);
    p_cancel = new Fl_Return_Button(400,380,120,25, "Cancel");
    p_cancel->callback((Fl_Callback *)cancel_partsCB,0);
    grp1[0]->end();
        
        
    }
    void show(){grp1[1]->show();}
    void hide () {grp1[1]->hide();}
   
   string torso() {return p_torso->value();}
   string head() {return p_head->value();}
   string batnum() {return p_batnum->value();}
   string battery() {return p_battery->value();}
   string arm() {return p_arm->value();}
   string locomotor() {return p_locomotor->value();}
   Fl_Input *p_torso;
   Fl_Input *p_batnum;
   Fl_Input *p_battery;
   Fl_Input *p_head;
   Fl_Input *p_arm;
   Fl_Input *p_locomotor;
   Fl_Return_Button *p_create;
   Fl_Return_Button *p_cancel;

};

//callback Order Dialog
void menu_create_orderCB(Fl_Widget* w, void* p){
    o_dlg->show();
}

void create_orderCB(Fl_Widget* w, void* p) {
   holder->porder("Torso: "+o_dlg->torso()+"\n Battery: "+o_dlg->batnum()+" "+o_dlg->battery()+"\n Head: "+o_dlg->head()+ "\n Arms:"+o_dlg->arm()+"\n Locomotor: "+o_dlg->locomotor());
   
    o_dlg->hide();
}
void cancel_orderCB(Fl_Widget* w, void* p) {
    o_dlg->hide();
}





//
//asscociate dialog
//

class Associate_Dialog{
public:
    Associate_Dialog(){
       
        grp1[2] = new Fl_Group(172,88,1024-200,768- 110,"Add Sales Associate");
        grp1[2]->box(FL_ENGRAVED_BOX);
        grp1[2]->align(FL_ALIGN_INSIDE|FL_ALIGN_TOP);
        grp1[2]->labelsize(24);
        a_name = new Fl_Input(170+140,160+00,200,20,"Name:");
        a_name->align(FL_ALIGN_LEFT);
        
        a_create = new Fl_Return_Button(200,240,120,25, "Create");
        a_create->callback((Fl_Callback *)create_associateCB,0);
        a_cancel = new Fl_Button(400,240,120,25, "Cancel");
        a_cancel->callback((Fl_Callback *)cancel_associateCB,0);
        grp1[1]->end();
        
        //grp1[1]->set_non_modal();
    }
    void show(){grp1[1]->show();}
    void hide () {grp1[1]->hide();}
    
    string saname() {return a_name->value();}
    


    Fl_Input *a_name;
    
    Fl_Return_Button *a_create;
    Fl_Button *a_cancel;
};

//callback associate Dialog
void menu_create_associateCB(Fl_Widget* w, void* p){
    a_dlg->show();
}

void create_associateCB(Fl_Widget* w, void* p) {
//    string nam;
//    cout<<"### Creating Sales Associate: "<<endl;
//    cout<<"Name            : "<<a_dlg->saname() <<endl;
    //nam = a_dlg->saname();
    
   holder->passociate(a_dlg->saname());

    a_dlg->hide();
}
void cancel_associateCB(Fl_Widget* w, void* p) {
    a_dlg->hide();
}


//
//customer Dialog
//



class Customer_Dialog{
public:
    Customer_Dialog(){
        //dialog = new Fl_Window(340,270,"Add New Customer");
        grp1[1] = new Fl_Group(172,88,1024-200,768- 110,"Create Customer");
        grp1[1]->box(FL_ENGRAVED_BOX);
        grp1[1]->align(FL_ALIGN_INSIDE|FL_ALIGN_TOP);
        grp1[1]->labelsize(24);
        c_name = new Fl_Input(170+140,160+00,200,20,"Name:");
        c_name->align(FL_ALIGN_LEFT);
        
        c_num = new Fl_Input(170+140,160+30,200,20,"Customer Number:");
        c_num->align(FL_ALIGN_LEFT);
        
        c_create = new Fl_Return_Button(200,240,120,25, "Create");
        c_create->callback((Fl_Callback *)create_customerCB,0);
        c_cancel = new Fl_Return_Button(400,240,120,25, "Cancel");
        c_cancel->callback((Fl_Callback *)cancel_customerCB,0);
        grp1[1]->end();
        
        //grp1[1]->set_non_modal();
    }
    void show(){grp1[1]->show();}
    void hide () {grp1[1]->hide();}
    
    string name() {return c_name->value();}
    string number() {return c_num->value();}
private:
    //Fl_Window *dialog;
    Fl_Input *c_name;
    Fl_Input *c_num;
    Fl_Return_Button *c_create;
    Fl_Return_Button *c_cancel;
};

//callback Customer Dialog
void menu_create_customerCB(Fl_Widget* w, void* p){
    c_dlg->show();
}

void create_customerCB(Fl_Widget* w, void* p) {
//    cout<<"### Creating Customer: "<<endl;
//    cout<<"Name            : "<<c_dlg->name() <<endl;
//    cout<<"Customer number : "<<c_dlg->number() <<endl;
   holder->pcustomer(c_dlg->name());
   holder->pcustnum(c_dlg->number());
    c_dlg->hide();
}
void cancel_customerCB(Fl_Widget* w, void* p) {
    c_dlg->hide();
}
//menu creator for drop menu and bar menu
class Toolbar : public Fl_Group {
public:
    Fl_Button *save1;
    Fl_Button *save2;
    Fl_Pixmap *p_save;
    Fl_Choice *font_group;
    Fl_Menu_Item *font_group_items;
    static void cb_save(Fl_Widget*, void*);
    static void cb_fonts(Fl_Widget*, void*);
    Toolbar(int Xpos, int Ypos, int Width, int Height);
};
void Toolbar::cb_save(Fl_Widget *w, void *data){
    fl_alert("Save Button %d pressed!",data);}
void Toolbar::cb_fonts(Fl_Widget *w, void *data){
    fl_alert("Menu number %d selected!",data);}
Toolbar::Toolbar(int Xpos, int Ypos, int Width, int Height) :
Fl_Group(Xpos, Ypos, Width, Height)
{
    box(FL_UP_BOX);
    Ypos += 2; Height -= 4; Xpos += 3; Width = Height;
    int i;
    save1 = new Fl_Button(Xpos, Ypos, Width, Height); Xpos += Width + 5;

    font_group = new Fl_Choice(Xpos, Ypos, 110, Height); Xpos += 111;
    p_save = new Fl_Pixmap(save_xpm);
    save1->image(p_save);

    save1->tooltip("Save file1");
    save1->callback(cb_save,(void*)1);


    font_group_items = new Fl_Menu_Item[4];
    for (i = 0; i < 4; i++) font_group_items[i].text = NULL;
    font_group_items->add("Employee", 0, cb_fonts, (void*) FONT_SERIF, 0);
    font_group_items->add("Customer", 0, cb_fonts,(void*) FONT_SANS_SERIF, 0);
    font_group_items->add("Manager", 0, cb_fonts, (void*) FONT_MONOSPACE, 0);
    font_group->menu(font_group_items);
}

//
//callbacks for create tab
//

void SelectGroup1_CB(Fl_Widget*, void *){
    int t;
    for(t=0;t<5;t++){
        //show the selected group
        if ( t == (bro->value()-1) ){grp1[t]->show();}
        else { grp1[t]->hide(); }
    }
}

//
//call backs for report
//
void SelectGroup2_CB(Fl_Widget*, void *){
    int t;
    for(t=0;t<4;t++){
        //show the selected group
        if ( t == (bro2->value()-1) ){grp2[t]->show();}
        else { grp2[t]->hide(); }
    }
}
//
//makes vertical tab
//
void ConstructTabChild1(Fl_Group *tab){
    //make browser screen
    bro=new Fl_Hold_Browser(10,80,150,768-100);
    bro->add("Order");
    bro->add("Customer");
    bro->add("Sales Associate");
    bro->add("Robot Part");
    bro->add("Robot Model");
    //make groups
    Order_Dialog();
    Customer_Dialog ();
    Associate_Dialog();
    Parts_Dialog();
    
    
    grp1[4] = new Fl_Group(172,88,1024-200,768- 110,"Robot Model");
    grp1[4]->box(FL_ENGRAVED_BOX);
    grp1[4]->align(FL_ALIGN_INSIDE|FL_ALIGN_TOP);
    grp1[4]->labelsize(24);
    new Fl_Input(170+140,160+00,200,20,"Torso:");
    new Fl_Input(170+140,160+30,200,20,"Head:");
    new Fl_Input(170+140,160+60,200,20,"Battery");
    new Fl_Input(170+140,160+90,200,20,"Arm:");
    grp1[4]->end();
    bro->callback(SelectGroup1_CB);
    
}
//
//makes second vertical tab group
//
void ConstructTabChild2(Fl_Group *tab){
    
    bro2=new Fl_Hold_Browser(10,80,150,768-100);
    bro2->add("Invoice");
    bro2->add("All Orders");
    bro2->add("Customer");
    bro2->add("Associate");
   
    grp2[0]=new Fl_Group(170,80,1024-200,768- 110,"Invoice");
    grp2[0]->box(FL_ENGRAVED_BOX);
    grp2[0]->labelsize(24);
    grp2[0]->align(FL_ALIGN_INSIDE|FL_ALIGN_TOP);
    
    new Fl_Output(170+25,80+40,1024-250,768-180,"");
    grp2[0]->end();
    grp2[1] = new Fl_Group(170,80,1024-200,768- 110,"All Orders");
    grp2[1]->box(FL_ENGRAVED_BOX);
    grp2[1]->align(FL_ALIGN_INSIDE|FL_ALIGN_TOP);
    grp2[1]->labelsize(24);
    new Fl_Output(170+25,80+40,1024-250,768-180,"");
    grp2[1]->end();
    grp2[2] = new Fl_Group(170,80,1024-200,768- 110,"All Customers");
    grp2[2]->box(FL_ENGRAVED_BOX);
    grp2[2]->align(FL_ALIGN_INSIDE|FL_ALIGN_TOP);
    grp2[2]->labelsize(24);
    new Fl_Output(170+25,80+40,1024-250,768-180,"");
    grp2[2]->end();
    grp2[3] = new Fl_Group(170,80,1024-200,768- 110,"All Associates");
    grp2[3]->box(FL_ENGRAVED_BOX);
    grp2[3]->align(FL_ALIGN_INSIDE|FL_ALIGN_TOP);
    grp2[3]->labelsize(24);
    new Fl_Output(170+25,80+40,1024-250,768-180,"");
    grp2[3]->end();
    bro2->callback(SelectGroup2_CB);
    
}
//
//makes top horzontal tabs
//

void ConstructTab(Fl_Group *tab) {
    static int a = 1;
    if(a==1){
        if ( !tab ) tab = new Fl_Group(10,80,1024-10,768-40, "create");     // create a new tab group
        tab->begin();                                 // begin adding widgets to the tab
        Fl_Box *b = new Fl_Box(10,80,1024-10,768-40);
        b->labelsize(50);                             // ..in big letters
        b->align(FL_ALIGN_INSIDE);                    // ..centered
        ConstructTabChild1(tab);
        
        
    }
    if(a==2){if ( !tab ) tab = new Fl_Group(10,80,1024-10,768-40, "report");     // create a new tab group
        tab->begin();                                 // begin adding widgets to the tab
        Fl_Box *b = new Fl_Box(10,80,1024-10,768-40);
        b->labelsize(50);                             // ..in big letters
        b->align(FL_ALIGN_INSIDE);                    // ..centered
        ConstructTabChild2(tab);
        
        
    }
    a++;
    tab->end();
}


//call back for bar meny
void menu_CB_logout(Fl_Widget * w, void*){win->label(" Selected");}
void menu_CB_Save(Fl_Widget * w, void*){win->label("Save Selected");}
void menu_CB_Quit(Fl_Widget * w, void*){win->label("Quit Selected");exit(0);}
void menu_CB_Help(Fl_Widget * w, void*){win->label("Help Selected");}
void menu_CB_Generate(Fl_Widget * w, void*){win->label("Generate Selected");holder->eggshell();}


int main(int argc, char *argv[]) {
    
    Fl::scheme("gtk+");
    
    const int width = 1024;
    const int height =768;
    //creates window
    win = new Fl_Window(width,height,"The Bot Shop");
    win->color(230);
    
    win->begin();
    // Create a horizontal tabs
    Fl_Tabs *tabs = new Fl_Tabs(8,58,width-20,height-70);
    {
        tabs->begin();
        ConstructTab(NULL);        // Tab 1
        ConstructTab(NULL);        // Tab 2
    }
    
    tabs->end();
      //create bar menu
    Fl_Menu_Bar menubar(0, 0, win->w(), 25);
    menubar.add("&File/&Save",0,menu_CB_Save);
    menubar.add("&File/&Quit",0, menu_CB_Quit);
    menubar.add("&Help",0,menu_CB_Help);
    menubar.add("&Help/generate",0,menu_CB_Generate);
    // Create the tab widget
    Toolbar tool(0,26,win->w(),30);
    
    tool.clear_visible_focus();
    
    
 
    SelectGroup1_CB(0,0);
    SelectGroup2_CB(0,0);
    win->resizable(win);
    win->end();
    win->show(argc, argv);
    return(Fl::run());
}

