//
//  Customer.hpp
//  robot
//
//  Created by Nohemi Gonzalez on 10/17/16.
//  Copyright © 2016 Nohemi Gonzalez Lopez. All rights reserved.
//

#ifndef customer_hpp
#define customer_hpp

#include <string>
#include <iostream>
using namespace std;


class Customer{
    string name;
    string custnumber;
public:
    Customer():name("unknown"),custnumber("unknown"){}
    Customer(string customer_name, string customer_number);
    string custto_string()const;
    string getcust(Customer& cust)const;
    string get_customername()const;
    string get_customernumber()const;

};
#endif /* Customer_hpp */
