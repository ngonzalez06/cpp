//
//  robot.hpp
//  shop
//
//  Created by Nohemi Gonzalez on 10/18/16.
//  Copyright © 2016 Nohemi Gonzalez Lopez. All rights reserved.
//

#ifndef robot_hpp
#define robot_hpp
//#include "std_lib_facilitie.h"
#include "Type.hpp"
#include <string>
#include <vector>
using namespace std;


class Robot{
public:
    string Description;
    string Arm;
    string Head;
    string Torso;
    string Batteries;
    string Locomotor;
    string Speed;
    string Locomotorpower;
    string Armpower;
    string Price;
    
    Robot(): Head(""), Torso(""), Batteries(""), Arm(""), Locomotor(""), Description(""), Price(""){}


    Robot(string r_head, string r_torso, string r_batteries, string r_arm, string r_locomotor, string r_description, string r_price);
    //getters;
   // Robot getrob();
    string get_getarm(Robot& rob)const;
    string get_getdescription(Robot& rop);
    string get_getprice(Robot& rop);
    string get_gethead(Robot& rop);
    string get_gettorso(Robot& rop);
    string get_getbatterties(Robot& rop);
    string get_getlocomotor(Robot& rop);
    string get_getarmpower(Robot& rop);
    string to_string(Robot& rob)const;
//    string desp();
//    string get_batteries(int index);
//    string get_torso(int index);
//    string get_locomotorpower( int index);
//    //string get_armpower(int index);
//    string get_head(int index);
//    string get_arm(int index);
//    string get_locomotor(int index);
//    string get_speed();
//    string get_getprice(Robot& rop);
    
    //setters
    
//    void add_locomoterpower(string power);
//    void add_armpower(string power);
//    void add_baterries(string addbat);
//    void add_torso(string addtoroso);
//    void add_head(string addhead);
//    void add_arm(string addarm);
//    void add_locomotor(string addlocomotor);
 //   void add_maxbat(int max);
//    //vector size
//    int maxbatnum(int i);
//    int headnum();
//    int torsonum();
//    int armnum();
//    int batterynum();
//    int locomotornum();
  
//    vector<int> maxbat;
//    vector<string> batteries;
//    vector<string> torso;
//    vector<string> head;
//    vector<string> arm;
//    vector<string> armpower;
//    vector<string> locomotorpower;
//    vector<string> locomotor;
    //print vector list
//    void printhead();
//    void printbat();
//    void printloco();
//    void printarm();
//    void printtoros();
//    
//    string choosearm(int a);
//    string choosetorso(int a);
//    string choosebattery(int a);
//    string chooselocomotor(int a);
//    string choosehead(int a);

    
    

 
};

#endif /* robot_hpp */
