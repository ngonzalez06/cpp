//
//  Controller.cpp
//  robot
//
//  Created by Nohemi Gonzalez on 10/12/16.
//  Copyright © 2016 Nohemi Gonzalez Lopez. All rights reserved.
//

#include "controller.hpp"
#include "view.hpp"
#include "customer.hpp"
#include "Shop.hpp"
//#include "componentType.hpp"
//#include "Components.hpp"
#include <iostream>
#include <string>


using namespace std;

void Controller::cli(){
    int cmd= 1;
    while(cmd!=0){
        view.show_menu();
        cout<<"Command? "<<endl;
    //cin.ignore();
        cin>>cmd;

        main_cmd(cmd);
    }
}
void Controller::main_cmd(int cmd){
    int choice=-1,quit=-1;
    
    switch(cmd){
        
        case 1:{
            while(choice != 0){
                view.create_menu();
                cout<<"Command? \n";
                cin.ignore();
                cin>>choice;
          
                create_cmd(choice);
            }
            choice = 1;
            break;
            
        }
        case 2:
            while(choice != 0){
                view.report_menu();
                cout<<"Command? "<<endl;
                cin.ignore();
                cin>>choice;
             
                report_cmd(choice);
            }
            choice=1;
            break;
    
        case 3:{
            //save
            
            break;}
        case 4:{
            shop.eggshell();
        }
        case 0:{
            cmd=-1;
            
            
            break;}
        default:
            cout<<"***Invalid Command***\n";
            break;
    }
    
}
void Controller::create_cmd(int cmd){
    string a,b,i, date, ordnum;
    int type, armtype, torsotype, headtype, locomotortype, choice, option, payment;
    Robot robot;
    switch(cmd){
            //creat order
        case 1:{
            Order neworder;
            
            cout<<"create order: "<<endl;
            cout<<"select customer"<<endl;
            view.list_customer();
            cin>>choice;
            if(choice == 0){
                cout<<"add new customer"<<endl;  cout<<"enter customer name: \n";
                cin>>a;
                cin.ignore();
                cout<<"enter customer number: \n";
                cin>>b;
                cin.ignore();
                Customer cust{a,b};
                shop.add_customer(cust);
            }
            cout<<"select customer"<<endl;
            view.list_customer();
            cin>>choice;
            Customer cust;
            cust = shop.customer_return(choice);
        
        
            shop.printrob();
            cin>>choice;
            Robot rob=shop.robot_tostring(choice);
            string price=rob.get_getprice(rob);
            
            cout<<"sold by"<<endl;
            view.list_associates();
            cin>>choice;
            Associate asso;
            asso=shop.associate_return(choice);
            cout<<"enter date: "<<endl;
            cin>>date;
            cout<<"enter rder number:"<<endl;
            cin>>ordnum;
            cout<<"enter date:"<<endl;
            cin>>date;
            cout<<"total amount of payment: "<<endl;
            cin>>payment;
            string bal=neworder.get_payment(price, payment);
            Order newo{cust, ordnum, price, bal, date, asso};
            
            
            break;
            
        }
        case 2:{
            cout<<"enter customer name: \n";
            cin>>a;
            cin.ignore();
            cout<<"enter customer number: \n";
            cin>>b;
            cin.ignore();
            Customer c1{a,b};
            shop.add_customer(c1);
            break;
        }
            
        case 3:{
            //string assname;
            cout<<"enter associate name: \n";
            cin>>a;
            cin.ignore();
            Associate a1{a};
            shop.add_associate(a1);
            break;
        }
            //create new model
        case 4:{ 
            int choice, counter=1, num;
            string tors, hea,ar,bat,loco,aaa;
            string model, type;
            
            cout<<"Create New Robot Model"<<endl;
            cout<<"You must have 5 different components."<<endl;
            //get torso//
            cout<<"choose torso "<<endl;
            for(int i=0; i<shop.torsonum();i++){
                type=shop.choosetorso(i);
                cout<<i<<" "<<type<<endl;
            }
            cin>>choice;
            tors=shop.choosetorso(choice);
            
            num=choice;
            //choose battery
            cout<<"choose "<<num<<" baterr(y)ies"<<endl;
            shop.printbat();
            cin>>choice;
            aaa=::to_string(num);
            bat =aaa + " " +shop.choosebattery(choice);
            
            
            cout<<"choose head"<<endl;
            //get head
            shop.printhead();
            cin>>choice;
            hea=shop.choosehead(choice);
            //get arms;
            shop.printarm();
            cin>>choice;
            ar=shop.choosearm(choice);
            //choose locomotor
            shop.printloco();
            cin>>choice;
            loco=shop.chooselocomotor(choice);
 
           
            cout<<"Enter Robot Model: "<<endl;
            cin>>model;
            //get price;
            string price;
            cout<<"enter price"<<endl;
            cin>>price;
            
            //create robot model
            
            Robot newrob{hea, tors, bat, ar, loco, model, price};
            shop.add_robot(newrob);
            
            break;
            
            }
            //add robot components
        case 5:{
      
            string type, power, armtype, torsotype, headtype, locomotortype, bat;

            int in=1, maxbat=1;
           

            while(in==1){
                view.component_list();
                cout<<"0: quit"<<endl;
                cout<<"\nenter choice: "<<endl;
               // cin.ignore();
                cin>>choice;
                switch(choice){
                    case 0:{
                        in=0;
                        break;
                    }
                    case 1:{
                        cout<<"add new torso name: "<<endl;
                        cin.ignore();
                        cin>>torsotype;
                     
                        cout<<"how many batteries: "<<endl;
                        cin.ignore();
                        cin>>maxbat;
                        while(maxbat>3){
                            cout<<"invalid enter a max battery at most 3";
                            cin>>maxbat;
                        }
                        string m= ::to_string(maxbat);
                        string aa= torsotype+ "max battery(ies)" +m;
                        shop.add_torso(aa);
                        shop.add_maxbat(maxbat);
                        break;
                    }
                    case 2:{
                        cout<<"add new Head"<<endl;
                        cin.ignore();
                        cin>>headtype;
                        shop.add_head(headtype);
                        
                        break;
                        
                    }
                    case 3:{
                        cout<<"add new arm"<<endl;
                        cin.ignore();
                        cin>>armtype;
                        cout<<"add arm power"<<endl;
                        string po,a;
                        cin>>po;
                        a=armtype+" power: "+po;
                        shop.add_arm(a);
                        break;
                    }
                    case 4:{
                        cout<<"add new Locomotor"<<endl;
                        cin.ignore();
                        cin>>locomotortype;
                        
                        cout<<"enter max speed"<<endl;
                        cin.ignore();
                        cin>>power;
                        string aa= locomotortype+" maxspeed:"+power;
                        shop.add_locomotor(aa);
                        shop.add_locomoterpower(power);
                        break;
                    }
                    case 5:{
                        cout<<"add new batteries"<<endl;
                        cin.ignore();
                        cin>>bat;
                        shop.add_baterries(bat);
                        break;
                    }
                    default:{
                        cout<<"invalid choice;"<<endl;
                        break;
                    }
                }
            }
            
        }
    }
}




    
void Controller::report_cmd(int cmd){
    
    switch(cmd){
        case 1:{
            shop.printorde();
            break;
        }
        case 2:{
            shop.printcustomer();
            //print list of customer
            break;
        }
        case 3:{
            shop.printassociate();
            break;}
            
        case 4:{
            //list robot models
            break;}
        case 5:{
            //list robot compoents
            int choice=1;
          
            while(choice!=0){
                view.component_list();
                cout<<"6: list all "<<endl;
                cin>>choice;
                Robot r;
                
                switch(cmd){
                    case 0:{
                        choice =0;
                        break;
                    }
                    case 1:{
                        //list torso
                        shop.printtoros();
                        break;
                    }
                    case 2:{
                        shop.printhead();
                        break;
                    }
                    case 3:{
                        shop.printarm();
                        break;
                        
                    }
                    case 4:{
                        shop.printloco();
                        break;
                    }
                    case 5:{
                        shop.printbat();
                        break;
                        
                    }
                    case 6:{
                        cout<<"Torso :"<<endl;
                        shop.printtoros();
                        cout<<"Head :"<<endl;
                        shop.printhead();
                        cout<<"Arms :"<<endl;
                        shop.printarm();
                        cout<<"Locomotor :"<<endl;
                        shop.printloco();
                        cout<<"Batteries :"<<endl;
                        shop.printbat();
                        break;
                    }
                }
            }
            
        }
            
            
        case 0:{
             cli();
            break;}
            
        default:{
            cout<<"***Invalid Command***\n";
            break;
        }

    }
}
