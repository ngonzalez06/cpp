//
//  main.c
//  gui
//
//  Created by Nohemi Gonzalez on 11/9/16.
//  Copyright © 2016 Nohemi Gonzalez Lopez. All rights reserved.
//
#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Box.H>
int main(int argc, char **argv) {
    Fl_Window *window = new Fl_Window(500,400);
    Fl_Box *box = new Fl_Box(10,5,30,10,"Shop store");
    box->box(FL_UP_BOX);
    box->labelfont(FL_BOLD+FL_ITALIC);
    box->labelsize(4);
    box->labeltype(FL_SHADOW_LABEL);
    window->end();
    window->show(argc, argv);
    return Fl::run();
}


//
//#include <FL/Fl.H>
//#include <FL/Fl_Double_Window.H>
//#include <string>
//
//using namespace std;
//
//int main(){
//    int w{Fl::w()/2}, h{Fl::h()};
//    string title{"part 1"}
//    Fl_Double_Window *window = new Fl_Double_Window(w, 0, w, h, title.c_str());
//    window->color(FL_Black);
//    window->show();
//    return Fl::run();
//    
//}
