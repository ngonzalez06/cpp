
 #include "std_lib_facilities.h"
 #include "age.h"

 int main() {
   bool passed = true;

   Age m1(Age::restricted);
   if (m1.to_string() != "restricted") {
     passed = false;
     cerr << "restricted failed: got " << m1.to_string() << endl;
   }

   cout << (passed ? "passed" : "failed") << endl;
 }
