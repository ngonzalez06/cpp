//
//  shop.hpp
//  shop
//
//  Created by Nohemi Gonzalez on 10/18/16.
//  Copyright © 2016 Nohemi Gonzalez Lopez. All rights reserved.
//

#ifndef shop_hpp
#define shop_hpp

#include <stdio.h>
#include "Header.h"
#include "Customer.hpp"
#include "robot.hpp"
#include "Associcate.hpp"


class Shop{
public:
    void add_robot(Robot rob);
    void add_order(Order ord);
    void add_customer(Customer cust);
    void add_associate(Associcate ass);
    
    void sale();
    
    int number_ofrobot();
    int number_ofcustomer();
    int number_ofassociate();
    int number_oforder();
    
    string robot_tostring(int index);
    string order_tostring(int index);
    string customer_tostring(int index);
    string associate_tostring(int index);
    
private:
    vector<Robot> robots;
    vector<Order> orders;
    vector<Customer> customers;
    vector<Associater> associates;
};
#endif /* shop_hpp */
