//
//  robot.hpp
//  shop
//
//  Created by Nohemi Gonzalez on 10/18/16.
//  Copyright © 2016 Nohemi Gonzalez Lopez. All rights reserved.
//

#ifndef robot_hpp
#define robot_hpp
#include "std_lib_facilities.h"
//#include <stdio.h>
//#include "Header.h"
//#include <string>
using namespace std;


class Robot{
public:
    Robot(string r_head, string r_torso, string r_batteries, string r_locomotor, string r_speed, string r_description, string r_locomotorpower, string r_armpower) : Head(r_head), Torso(r_torso), Batteries(r_batteries), Locomotor(r_locomotor), Speed(r_speed), Description(r_description),  Locomotorpower(r_locomotorpower), Armpower(r_armpower){}
                                                                                                                                                                                                                                                                                                                                  
    string desp();
    string get_batteries(int index);
    string get_torso(int index);
    string get_locomotorpower( int index);
    string get_armpower(int index);
    string get_head(int index);
    string get_arm(int index);
    string get_locomotor(int index);
    string get_speed();
    void add_locomoterpower(string power);
    void add_armpower(string power);
    void add_baterries(string addbat);
    void add_torso(string addtoroso);
    void add_head(string addhead);
    void add_arm(string addarm);
    void add_locomotor(string addlocomotor);
    
    
    vector<string> batteries;
    vector<string> torso;
    vector<string> head;
    vector<string> arm;
    vector<string> armpower;
    vector<string> locomotorpower;
    vector<string> locomotor;
    
private:
    string Description;
                                                                                                                                                                                                                                                                                                                                  
    string Head;
    string Torso;
    string Batteries;
    string Locomotor;
    string Speed;
    string Locomotorpower;
    string Armpower;
 
};

#endif /* robot_hpp */
