//
//  catalog.hpp
//  library
//
//  Created by Nohemi Gonzalez on 9/17/16.
//  Copyright © 2016 Nohemi Gonzalez. All rights reserved.
//


#ifndef catalog_hpp
#define catalog_hpp
#endif /* catalog_hpp */
#include <vector>
#include <stdio.h>
#include <string>
using namespace std;
class Catalog{
  public:
    string iString,jString;  
    //Catalog(string i, string j);
    vector<string> title, author, copyright, media, audience, isbn;
    vector<bool> checkedout;
    string get_Name(int index);
    void get_patron(string n);
    void get_phone(string p);
    void addtitle(string t);
    void addauthor(string a);
    void addmedia(string m);
    void addcopyright(string co);
    void addaudience(string au);
    void addisbn(string i);
    void addcheck(bool a);
    unsigned int lookup(string name);
    void list();
    void create();
    void check_out(unsigned int a, string first, string last);
    void check_in(unsigned int b);
    void noPatron();
    bool searchisbn(string search);
    bool operator==(const Catalog& i)const;
    unsigned int sizelook();
    vector<string> getisbn();
  private:
    vector<string> patron, phone;
};
