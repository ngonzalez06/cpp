//  Associate.hpp
//  robot
//  Created by Nohemi Gonzalez on 10/12/16.
//  Copyright © 2016 Nohemi Gonzalez Lopez. All rights reserved.
#ifndef Associate_hpp
#define Associate_hpp
//#include "std_lib_facilities.h"


#include "Shop.hpp"
#include "std_lib_facilities.h"
//#include <string>

using namespace std;

class Associate{
    public:
    Associate(string Associate_name):name(Associate_name){}
    Associate() : name("unknown"){}

    string to_string();
    string get_assname();
    private:
        string name;

};
#endif /* Associate_hpp */
