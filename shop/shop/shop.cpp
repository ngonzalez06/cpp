//
//  shop.cpp
//  shop
//
//  Created by Nohemi Gonzalez on 10/18/16.
//  Copyright © 2016 Nohemi Gonzalez Lopez. All rights reserved.
//

#include "shop.hpp"
#include "Header.h"



    void Shop::add_robot(Robot rob){robots.push_back(rob);}
    void Shop::add_order(Order ord){orders.push_back(ord);}
    void Shop::add_associate(Associate ass){associates.push_back(ass);}
    void Shop::add_customer(Customer cust){customers.push_back(cust);}
    
    int Shop::number_ofrobot(){return robot.size()};
    int Shop::number_ofcustomer(){ return customer.size();}
    int Shop::number_ofassociate(){return associcate.size();}
    int Shop::number_oforder(){return order.size;}



string Shop::robot_tostring(int index){return robots[index] ;}
string Shop::order_tostring(int index){return order[index];}
string Shop::customer_tostring(int index){return customer[index];}
string Shop::associate_tostring(int index){return associate[index];}
