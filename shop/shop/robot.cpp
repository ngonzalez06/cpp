//
//  robot.cpp
//  shop
//
//  Created by Nohemi Gonzalez on 10/18/16.
//  Copyright © 2016 Nohemi Gonzalez Lopez. All rights reserved.
//

#include "robot.hpp"

string Robot::get_batteries(int index){return batteries[index];}
string Robot::get_torso(int index){return torso[index];}
string Robot::get_head(int index){return head[index];}
string Robot::get_arm(int index){return arm[index];}
string Robot::get_locomotor(int index){return locomotor[index];}
string Robot::get_locomotorpower( int index){return locomotor[index];}
string Robot::get_armpower(int index){return armpower[index];}
string Robot::desp(){
    string descript;
    cout<<"enter a description"<<endl;
    cin>>descript;
    cin.ignore();
    return descript;
}
string Robot::get_speed(){
    string speed;
    cout<<"enter speed"<<endl;
    cin>>speed;
    cin.ignore();
    return speed;
}
void Robot::add_baterries(string addbat){batteries.push_back(addbat);}
void Robot::add_torso(string addtor){torso.push_back(addtor);}
void Robot::add_head(string addhead){head.push_back(addhead);}
void Robot::add_arm(string addarm){arm.push_back(addarm);}
void Robot::add_locomotor(string addlocomotor){locomotor.push_back(addlocomotor);}
void Robot::add_locomoterpower(string power){locomotorpower.push_back(power);}
void Robot::add_armpower(string power){armpower.push_back(power);}
