//
//  View.hpp
//  robot
//
//  Created by Nohemi Gonzalez on 10/12/16.
//  Copyright © 2016 Nohemi Gonzalez Lopez. All rights reserved.
//

#ifndef View_hpp
#define View_hpp
#include "std_lib_facilities.h"
#include <stdio.h>
#include "View.hpp"
#include "shop.hpp"
#include "Header.h"
#include "Associate.hpp"


class View{
    public:
    View(Shop& shp):shop(shp){}
        void show_menu();
        void create_menu();
        void report_menu();
        void list_associates();
        void list_customer();
        void list_componenttype();
    private:
        Shop& shop;
};
#endif /* View_hpp */
