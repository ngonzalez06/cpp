//
//  Controller.hpp
//  robot
//
//  Created by Nohemi Gonzalez on 10/12/16.
//  Copyright © 2016 Nohemi Gonzalez Lopez. All rights reserved.
//

#ifndef Controller_hpp
#define Controller_hpp

#include <stdio.h>
#include "Header.H"
#include "Shop.hpp"
#include "View.hpp"

class Controller{
    public:
        Controller(Shop& shp) : shop(shp),view(View(shop)){}
        void cli();
        void main_cmd(char cmd);
        void create_cmd(char cmd);
        void report_cmd(char cmd);
        
    private:
        Shop& shop;
        View view;
};
#endif /* Controller_hpp */
