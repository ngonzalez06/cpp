#include "Student.h"
#include "Roster.h"
#include <iostream>

using namespace std;

int main() {
    Student s1 {"Cecelia", 3.8 };
    Student s2 {"Sam", 3.1};

//    cout << s1 << endl; 
//    cout << s2 << endl;
    Roster roster;
    roster.add(s1);
    roster.add(s2);

//    //cout << roster << endl;
//    cout << roster.find("Cecelia") << endl;
//    cout << roster.find("Sam") << endl;
//    cout << roster.find("Max") << endl;

}
