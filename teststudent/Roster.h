#ifndef STUDENTDB_ROSTER_H
#define STUDENTDB_ROSTER_H

#include "Student.h"
#include <vector>
#include <iostream>

using namespace std;

class Roster {
    vector<Student> students;
public:
    Roster();

    void add(Student& student);

    Student find(string name) const;

    friend ostream& operator<<(ostream& ostr, const Roster& roster);
};

ostream& operator<<(ostream& ostr, const Roster& roster);

#endif //STUDENTDB_ROSTER_H
