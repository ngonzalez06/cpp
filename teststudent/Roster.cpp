
#include <iterator>
#include <algorithm>
#include "Roster.h"

Roster::Roster() { }

void Roster::add(Student& student) {
    students.push_back(student);
}

Student Roster::find(string name) const {
//    for (int i = 0; i < students.size(); i++) {
//        if (students.at(i).get_name() == name) {
//            return students.at(i);
//        }
//    } // end for loop
    for (auto student : students) {
        if (student.get_name() == name) {
            return student;
        }
    } // end for loop
//
//    auto name_match_fn = [name](auto student) {
//        return student.get_name() == name;
//    };

//    auto student_it = find_if(begin(students), end(students), name_match_fn);
//    if (student_it == end(students)) {
//        // no match found
        return Student();
//    }
//    else {
//        return *student_it;
//    }
}

/**
 * Overloaded output operator for Roster.
 * @param ostr the output stream to write to.
 * @param roster the Roster to output.
 * @return the (modified) ostream.
 */
ostream& operator<<(ostream& ostr, const Roster& roster) {
    ostr << "{ ";

    // copy up to but not including the last element
    // putting a "; " after each one
    copy(begin(roster.students), end(roster.students) - 1,
         ostream_iterator<Student>(ostr, "; "));

    // now output the last element (without a trailing "; ")
    if (roster.students.size() > 0) {
        ostr << roster.students.at(roster.students.size() - 1) << ' ';
    }

    ostr << "}";
    return ostr;
}
