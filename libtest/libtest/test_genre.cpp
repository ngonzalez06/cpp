
 #include "std_lib_facilities.h"
 #include "genre.h"

 int main() {
   bool passed = true;

   Genre m1(Genre::performance);
   if (m1.to_string() != "performance") {
     passed = false;
     cerr << "performance failed: got " << m1.to_string() << endl;
   }

   cout << (passed ? "passed" : "failed") << endl;
 }
