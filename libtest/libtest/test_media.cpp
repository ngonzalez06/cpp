
 #include "std_lib_facilities.h"
 #include "media.h"

 int main() {
   bool passed = true;

   Media m1(Media::video);
   if (m1.to_string() != "video") {
     passed = false;
     cerr << "video failed: got " << m1.to_string() << endl;
   }

   cout << (passed ? "passed" : "failed") << endl;
 }
