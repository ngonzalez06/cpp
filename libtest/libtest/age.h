
 #ifndef __AGE_H
 #define __AGE_H 201609
 #include "string"

 class Age {
   public:
     Age(int val) : value(val) { }

     static const int children = 0;
     static const int teen = 1;
     static const int adult = 2;
     static const int restricted = 3;

     static const int num_ages = 4;

     string to_string() {
       switch(value) {
         case(children):return "children";
         case(teen):return "teen";
         case(adult):return "adult";
         case(restricted):return "restricted";
         default: return "UNKNOWN";
       }
     }
   private:
     int value;
 };
 #endif



 #ifndef __GENRE_H
 #define __GENRE_H 201609
 #include "string"

 class Genre {
   public:
     Genre(int val) : value(val) { }

     static const int fiction = 0;
     static const int nonfiction = 1;
     static const int selfhelp = 2;
     static const int performance = 3;

     static const int num_genres = 4;

     string to_string() {
       switch(value) {
         case(fiction):return "fiction";
         case(nonfiction):return "nonfiction";
         case(selfhelp):return "selfhelp";
         case(performance):return "performance";
         default: return "UNKNOWN";
       }
     }
   private:
     int value;
 };
 #endif



#ifndef __CONTROLLER_H
#define __CONTROLLER_H 201609
#include "library.h"
#include "view.h"

class Controller {
  public:
    Controller (Library& lib) : library(lib), view(View(library)) { }
    void cli();
    void execute_cmd(int cmd);
  private:
    Library& library; 
    View view;
};
#endif

#include "controller.h"
#include "view.h"
#include "library.h"
#include "publication.h"
#include "patron.h"
#include "genre.h"
#include "media.h"
#include "age.h"
#include <iostream>
#include <string>
using namespace std;
void Controller::cli() {
  int cmd = -1;
  while (cmd != 0) {
    view.show_menu();
    cout << "Command? ";
    cin >> cmd;
    cin.ignore(); // consume \n
    execute_cmd(cmd);
  }
}
void Controller::execute_cmd(int cmd) {
  if (cmd == 1) { // Add publication
    string title, author, copyright, isbn;
    int genre, media, age;

    cout << "Title? ";
    getline(cin, title);

    cout << "Author? ";
    getline(cin, author);

    cout << "Copyright date? ";
    getline(cin, copyright);

    for (int i = 0; i < Genre::num_genres; ++i) 
      cout << "  " << i << ") " << Genre(i).to_string() << endl;
    cout << "Genre? ";
    cin >> genre;
    cin.ignore(); // consume \n

    for (int i = 0; i < Media::num_media; ++i) 
      cout << "  " << i << ") " << Media(i).to_string() << endl;
    cout << "Media? ";
    cin >> media;
    cin.ignore(); // consume \n
    for (int i = 0; i < Age::num_ages; ++i) 
      cout << "  " << i << ") " << Age(i).to_string() << endl;
    cout << "Age? ";
    cin >> age;
    cin.ignore(); // consume \n

    cout << "ISBN? ";
    getline(cin, isbn);

    library.add_publication(Publication(title, author, copyright, genre, media, age, isbn));

 } else if (cmd == 2) { // List all publications
    view.list_publications();

 } else if (cmd == 3) { // Check out publication
    int pub, pat;

    view.list_publications();
    cout << "Publication number? ";
    cin >> pub;
    cin.ignore(); // consume \n

    view.list_patrons();
    cout << "Patron number? ";
    cin >> pat;
    cin.ignore(); // consume \n

    library.check_out(pub, pat);
    
 } else if (cmd == 4) { // Check in publication
    int pub;
    view.list_publications();
    cout << "Publication number? ";
    cin >> pub;
    cin.ignore(); // consume \n

    library.check_in(pub);

 } else if (cmd == 5) { // Add patron
    string name, number;

    cout << "Name? ";
    getline(cin, name);
    cout << "Phone number? ";
    getline(cin, number);
    library.add_patron(Patron(name, number));
    
 } else if (cmd == 6) { // List all patrons
    view.list_patrons();

 } else if (cmd == 9) { // Help
    view.help();
 } else if (cmd == 0) { // Exit
 } else if (cmd == 99) { // Easter Egg
   library.easter_egg();
 } else {
   cerr << "**** Invalid command - type 9 for help" << endl << endl;
 }
}
#ifndef __LIBRARY_H
 #define __LIBRARY_H 201609
 #include "patron.h"
 #include "publication.h"

 #include <iostream>
 #include <string>
 #include <vector>

 using namespace std;


 class Library {
   public:
     void add_publication(Publication pub);
     void add_patron(Patron pat);

     void check_out(int publication_index, int patron_index);
     void check_in(int publication_index);

     string publication_to_string(int publication_index);
     string patron_to_string(int patron_index);

     int number_of_publications();
     int number_of_patrons();

     void easter_egg();
   private:
     vector<Publication> publications;
     vector<Patron> patrons;
 };
 #endif

#include "library.h"

void Library::add_publication(Publication pub) {
  publications.push_back(pub);
}

void Library::add_patron(Patron pat) {
  patrons.push_back(pat);
}

void Library::check_out(int publication_index, int patron_index) {
  publications[publication_index].check_out(patrons[patron_index]);
}

void Library::check_in(int publication_index) {
  publications[publication_index].check_in();
}

string Library::publication_to_string(int publication_index) {
  return publications[publication_index].to_string();
}

string Library::patron_to_string(int patron_index) {
  return patrons[patron_index].to_string();
}

int Library::number_of_publications() {
  return publications.size();
}

int Library::number_of_patrons() {
  return patrons.size();
}

void Library::easter_egg() {
 add_publication(Publication("The Firm", "John Grisham", "1991",
       Genre::fiction, Media::book, Age::adult, "0440245923"));
 add_publication(Publication("Foundation", "Isaac Asimov", "1942",
        Genre::fiction, Media::book, Age::adult, "0385177259"));
 add_publication(Publication("Foundation and Empire", "Isaac Asimov", "1943",
        Genre::fiction, Media::book, Age::adult, "0385177259"));
 add_publication(Publication("Second Foundation", "Isaac Asimov", "1944",
        Genre::fiction, Media::book, Age::adult, "0385177259"));
 add_publication(Publication("War of the Worlds", "Jeff Wayne", "1977",
        Genre::performance, Media::audio, Age::teen, "9780711969148"));
 add_publication(Publication("Willy Wonka and the Chocolate Factory", "Roald Dahl", "1971",
        Genre::performance, Media::video, Age::children, "0142410314"));
 add_patron(Patron("Larry", "817-555-1111"));
 add_patron(Patron("Curly", "817-555-2222"));
 add_patron(Patron("Moe", "817-555-3333"));
}

#include "controller.h"
#include "library.h"

int main() {
  Library library;
  Controller controller(library);
  controller.cli();
}
 #ifndef __MEDIA_H
 #define __MEDIA_H 201609
 #include "string"

 class Media {
   public:
     Media(int val) : value(val) { }

     static const int book = 0;
     static const int periodical = 1;
     static const int newspaper = 2;
     static const int audio = 3;
     static const int video = 4;

     static const int num_media = 5;

     string to_string() {
       switch(value) {
         case(book):return "book";
         case(periodical):return "periodical";
         case(newspaper):return "newspaper";
         case(audio):return "audio";
         case(video):return "video";
         default: return "UNKNOWN";
       }
     }
   private:
     int value;
 };
 #endif
#ifndef __PATRON_H
 #define __PATRON_H 201609
 #include <iostream>
 #include <string>

 using namespace std;

 class Patron {
   public:
     Patron(string patron_name, string patron_phone_number) 
       : name(patron_name), number(patron_phone_number) {}
     Patron() : name("unknown"), number("unknown") {}

     string to_string();

     string get_patron_name();
     string get_patron_phone_number();

   private:
     string name;
     string number;
 };
 #endif

#include "patron.h"

 string Patron::to_string() {return name + " (" + number + ")";}

 string Patron::get_patron_name() {return name;}
 string Patron::get_patron_phone_number() {return number;}




#ifndef __PUBLICATION_H
 #define __PUBLICATION_H 201609
 #include "patron.h"
 
 #include "media.h"
 #include "genre.h"
 #include "age.h"

 #include <iostream>
 #include <string>

 using namespace std;


 class Publication {
   public:
     Publication(string p_title, 
                 string p_author, 
                 string p_copyright,
                 Genre p_genre, 
                 Media p_media, 
                 Age p_target_age,
                 string p_isbn) : 

                 title(p_title), 
                 author(p_author),
                 copyright(p_copyright), 
                 genre(p_genre), 
                 media(p_media),
                 target_age(p_target_age), 
                 isbn(p_isbn), 
                 patron(Patron()),
                 checked_out(false) { }

     bool is_checked_out();

     void check_out(Patron& patron);
     void check_in();

     string to_string();

     friend bool operator==(const Publication& p1, const Publication& p2);
     friend bool operator!=(const Publication& p1, const Publication& p2);
     friend ostream& operator<<(ostream& os, Publication& p);

     // Thrown on check_in if publication isn't checked out
     //   or on cheeck_out if publication is already checked out
     class Invalid_transaction { }; 

   private:
     string title;
     string author;
     string copyright;
     Genre genre;
     Media media;
     Age target_age;
     string isbn;
     Patron patron;
     bool checked_out;
 };
#ifdef _MSC_VER
 #include "stdafx.h"
 #endif
 #include "publication.h"
 #include <string>
 #include <iostream>

 using namespace std;


 bool Publication::is_checked_out() {return checked_out;}
 void Publication::check_out(Patron& p_patron) {
   if (checked_out) {
     throw Invalid_transaction();
   } else {
     patron = p_patron;
     checked_out = true;
   }
 }
   
 void Publication::check_in() {
   if (checked_out) {
     checked_out = false;
   } else {
     throw Invalid_transaction();
   }
 }

 string Publication::to_string() {
   string pub = "\"" + title + "\"" + " by " + author + ", " + copyright + 
     " (" + target_age.to_string() + " " + genre.to_string() + " "  
         + media.to_string() + ") " + "ISBN: " + isbn;
   if (checked_out) {
      pub += "\nChecked out to " + patron.get_patron_name() + 
             " (" + patron.get_patron_phone_number() + ")";
   }
   return pub;
 }

 bool operator==(const Publication& p1, const Publication& p2) {
     return (p1.isbn == p2.isbn);
 }

 bool operator!=(const Publication& p1, const Publication& p2) {
     return !(p1 == p2);
 }

 ostream& operator<<(ostream& os, Publication& p) {
     os << p.to_string();
     return os;
 }
#ifndef __VIEW_H
#define __VIEW_H 201609
 
#include "library.h"

class View {
  public:
    View(Library& lib) : library(lib) { }
    void show_menu();
    void list_publications();
    void list_patrons();
    void help();
  private:
    Library& library; 
};
#endif
#include "view.h"
#include <iostream>
#include <string>

using namespace std;a

void View::show_menu() {
  cout << endl << endl;
  cout << "===============================" << endl;
  cout << "C1325 Library Management System" << endl;
  cout << "===============================" << endl;
  cout << endl;
  cout << "Publications" << endl;
  cout << "------------" << endl;
  cout << "(1) Add publication" << endl;
  cout << "(2) List all publications" << endl;
  cout << "(3) Check out publication" << endl;
  cout << "(4) Check in publication" << endl;
  cout << endl;
  cout << "Patrons" << endl;
  cout << "-------" << endl;
  cout << "(5) Add patron" << endl;
  cout << "(6) List all patrons" << endl;
  cout << endl;
  cout << "Utility" << endl;
  cout << "-------" << endl;
  cout << "(9) Help" << endl;
  cout << "(0) Exit" << endl;
  cout << endl;
}

void View::list_publications() {
  cout << endl;
  cout << "----------------------------" << endl;
  cout << "List of Library Publications" << endl;
  cout << "----------------------------" << endl;
  for (int i=0; i<library.number_of_publications(); ++i) {
    cout << i << ") " << library.publication_to_string(i) << endl;
  }
}

void View::list_patrons() {
  cout << endl;
  cout << "-----------------------" << endl;
  cout << "List of Beloved Patrons" << endl;
  cout << "-----------------------" << endl;
  for (int i=0; i<library.number_of_patrons(); ++i) {
    cout << i << ") " << library.patron_to_string(i) << endl;
  }
}

void View::help() {
  cout << "Try harder." << endl;
}
