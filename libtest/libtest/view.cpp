#include "view.h"
#include <iostream>
#include <string>

using namespace std;

void View::show_menu() {
  cout << endl << endl;
  cout << "===============================" << endl;
  cout << "C1325 Library Management System" << endl;
  cout << "===============================" << endl;
  cout << endl;
  cout << "Publications" << endl;
  cout << "------------" << endl;
  cout << "(1) Add publication" << endl;
  cout << "(2) List all publications" << endl;
  cout << "(3) Check out publication" << endl;
  cout << "(4) Check in publication" << endl;
  cout << endl;
  cout << "Patrons" << endl;
  cout << "-------" << endl;
  cout << "(5) Add patron" << endl;
  cout << "(6) List all patrons" << endl;
  cout << endl;
  cout << "Utility" << endl;
  cout << "-------" << endl;
  cout << "(9) Help" << endl;
  cout << "(0) Exit" << endl;
  cout << endl;
}

void View::list_publications() {
  cout << endl;
  cout << "----------------------------" << endl;
  cout << "List of Library Publications" << endl;
  cout << "----------------------------" << endl;
  for (int i=0; i<library.number_of_publications(); ++i) {
    cout << i << ") " << library.publication_to_string(i) << endl;
  }
}

void View::list_patrons() {
  cout << endl;
  cout << "-----------------------" << endl;
  cout << "List of Beloved Patrons" << endl;
  cout << "-----------------------" << endl;
  for (int i=0; i<library.number_of_patrons(); ++i) {
    cout << i << ") " << library.patron_to_string(i) << endl;
  }
}

void View::help() {
  cout << "Try harder." << endl;
}
