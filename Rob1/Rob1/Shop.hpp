//
//  shop.h
//  robot
//
//  Created by Nohemi Gonzalez on 10/12/16.
//  Copyright © 2016 Nohemi Gonzalez Lopez. All rights reserved.
//
#ifndef Shop_hpp
#define Shop_hpp

#include <stdio.h>
#include <vector>
#include "Robot.hpp"
//#include "Order.hpp"
#include "Associate.hpp"
#include "Customer.hpp"
#include "ComponentType.hpp"
#include "Arm.hpp"

class Shop{
    public:
        void add_robot(Robot* rob);
        //void add_order(Order ord);
        void add_associate(Associate ass);
        void add_customer(Customer cust);
      //  void add_arm(Arm ar);
        void add_componenettype(CompenentType c_type);
    
        string robot_tostring(int robot_index);
        //string order_tostring(int order_index);
        string associate_tostring(int sales_index);
        string customer_tostring(int customer_index);
        string Componenttype_tostring(int comp_index);
        //string (armtype_tostring);
    
    
    
        int numberof_robot();
        int numberof_order();
        int numberof_associate();
        int numberof_customer();
        int numberof_arm();
        int numberof_componenttype();
    private:
        vector<Robot> robot;
     //   vector<Order> order;
        vector<Associate> associate;
        vector<Customer> customer;
        vector<ComponentType> componenttype;
        vector<Arm> arm;

};
#endif /* Shop_hpp */
