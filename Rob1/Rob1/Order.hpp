//
//  Order.hpp
//  robot
//
//  Created by Nohemi Gonzalez on 10/12/16.
//  Copyright © 2016 Nohemi Gonzalez Lopez. All rights reserved.
//

#ifndef Order_hpp
#define Order_hpp

#include <stdio.h>
#include "std_lib_facilities.h"
#include <string>
#include "Customer.hpp"
#include "Associate.hpp"
#include "Shop.hpp"
#include <iostream>



class Order{
    public:
    Order( Customer o_customer, string o_price,  string o_balance, string o_date, Associate a_associate, balance o_bal): customer(o_customer),price(o_price), balance(o_balance), date(o_date), associcate(a_associcate), balance(o_bal){}
    
    string to_string();
    
    bool has_balance();
    void bal();
    void robot_part();
private:
//string order_number;
    string price;
    string date;
    string payment;
    Associate associcate;
    Customer customer;
    string balance;
    
    
};

#endif /* Order_hpp */
