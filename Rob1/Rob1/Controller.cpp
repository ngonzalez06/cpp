//
//  Controller.cpp
//  robot
//
//  Created by Nohemi Gonzalez on 10/12/16.
//  Copyright © 2016 Nohemi Gonzalez Lopez. All rights reserved.
//


#include "Controller.hpp"
#include "View.hpp"
#include "Customer.hpp"
#include "Shop.hpp"
#include "ComponentType.hpp"
#include "Components.hpp"
#include <cctype>
#include <iostream>
#include <string>


using namespace std;

void Controller::cli(){
    char cmd= 'a';
    while(cmd!='Q'){
        view.show_menu();
        cout<<"Command? ";
        cin>>cmd;
        cin.ignore();
        main_cmd(cmd);
    }
}
void Controller::main_cmd(char cmd){
    char choice='a';
    cmd=toupper(cmd);
    switch(cmd){
        case 'C':
            while(choice != 'Q'){
                view.create_menu();
                cout<<"Command? \n";
                cin>>choice;
                cin.ignore();
                create_cmd(choice);
            }
            choice = 'a';
            break;
            
    
        case 'R':
            while(choice != 'Q'){
                view.report_menu();
                cout<<"Command? \n";
                cin>>choice;
                cin.ignore();
                report_cmd(choice);
            }
            choice='a';
            break;
    
        case 'S':
            
            break;
        case 'Q':
            
            break;
        default:
            cout<<"***Invalid Command***\n";
            
    }
}
void Controller::create_cmd(char cmd){
    cmd=toupper(cmd);
    int type, armtype, torsotype, headtype, locomotortype;
    switch(cmd){
        case 'O':
            
            break;
        case 'C':
            string customername, custnumber;
            cout<<"enter customer name: \n";
            cin>>customername;
            cin.ignore();
            cout<<"enter customer number: \n";
            cin>>custnumber;
            cin.ignore();
            shop.add_customer(Customer(customername,custnumber));
            break;
        case 'S':
            string assname;
            cout<<"enter associate name: \n";
            cin>>assname;
            cin.ignore();
            shop.add_associate(Associate(assname));
            break;
        case 'M':
            
        
            break;
        case 'R':
            for (int i = 0; i < ComponentType::num_type; ++i)
                cout << "  " << i << ") " << ComponentType(i).to_string() << endl;
            cout << "Component type? ";
            cin >> type;
            cin.ignore(); // consume \n
            switch(type){
                case ComponentType::arm :
                    cout<<"choose arm: \n"
            }

            break;
        case 'Q':
        
            break;
        default:
            cout<<"***Invalid Command***\n";
            
            
    }
    
    
}
void Controller::report_cmd(char cmd){
    cmd=toupper(cmd);
    switch(cmd){
        case 'O':
            
            break;
        case 'C':
            
            break;
        case 'S':
            view.list_associates();
            
            
            break;
        case 'M':
            
            break;
        case 'P':
            
            break;
        case 'Q':
            
            break;
        default:
            cout<<"***Invalid Command***\n";

    }
    
    

}
