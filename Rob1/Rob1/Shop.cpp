//
//  shop.c
//  robot
//
//  Created by Nohemi Gonzalez on 10/12/16.
//  Copyright © 2016 Nohemi Gonzalez Lopez. All rights reserved.
//


#include "Shop.hpp"
#include <string>
using namespace std;
void Shop::add_robot(Robot* rob){robot.push_back(rob);}
//void Shop::add_order(Order ord){order.push_back(ord);}
void Shop::add_associate(Associate ass){associate.push_back(ass);}
void Shop::add_customer(Customer cust){customer.push_back(cust);}
//void Shop::add_arm(Arm ar){arm.push_back(ar);}
//void Shop::add_componenettype(ComponentType c_type){componenttype.push_back;}

//string Shop::arm_tostring(int arm_index){
//    return arm[i].tostring();
//}

//string Shop::robot_tostring(int robot_index){
//    return robot[robot_index]->to_string();
//    
//}
string Shop::order_tostring(int order_index){
    return order[order_index].to_string();

}
string Shop::associate_tostring(int sales_index){
    return associate[sales_index].to_string();

}
//string Shop::customer_tostring(<#int customer_index#>){
//    return customer[customer_index].to_string();}
int Shop::numberof_robot(){
    return robot.size();
}
int Shop::numberof_order(){
    return order.size();

}
int Shop::numberof_associate(){
    return associate.size();
}
int Shop::numberof_customer(){return customer.size();}
int Shop::numberof_arm(){return arm.size();}
//int Shop::numberof_componenettype(){return componenetype.size();}
