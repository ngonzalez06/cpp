//
//  Order.cpp
//  robot
//
//  Created by Nohemi Gonzalez on 10/12/16.
//  Copyright © 2016 Nohemi Gonzalez Lopez. All rights reserved.
//
//
#include "Order.hpp"
#include "std_lib_facilities.h"

string Order::to_string(){
    string ord = customer.to_string()+ " " +date+ " " +associate.to_string();
    
    return ord;
}
