//
//  Components.hpp
//  robot
//
//  Created by Nohemi Gonzalez on 10/17/16.
//  Copyright © 2016 Nohemi Gonzalez Lopez. All rights reserved.
//

#ifndef Components_hpp
#define Components_hpp
#include "std_lib_facilities.h"
#include <stdio.h>
#include "ComponentType.hpp"
#include <string>
using namespace std;

class Components{
public:
    Components (string c_name,
               string c_partnum,
               ComponentType c_type,
               string c_weight,
               string c_cost,
               string c_description) :
    
                name(c_name),
                partnum(c_partnum),
                componenttype(c_type),
                weight(c_weight),
                cost(c_cost),
                description(c_description){}
    
    
    string to_string();
    
    

private:
    string name;
    string partnum;
    ComponentType componenttype;
    string weight;
    string cost;
    string description;

};
#endif /* Components_hpp */
