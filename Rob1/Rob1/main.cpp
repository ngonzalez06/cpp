//
//  main.cpp
//  robot
//
//  Created by Nohemi Gonzalez on 10/17/16.
//  Copyright © 2016 Nohemi Gonzalez Lopez. All rights reserved.
//

#include <stdio.h>
#include "Controller.hpp"
#include "Shop.hpp"
int main(){
    Shop shop;
    Controller controller(shop);
    controller.cli();
    
    return 0;
}

