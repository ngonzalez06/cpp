//
//  ComponentType.hpp
//  robot
//
//  Created by Nohemi Gonzalez on 10/17/16.
//  Copyright © 2016 Nohemi Gonzalez Lopez. All rights reserved.
//

#ifndef ComponentType_hpp
#define ComponentType_hpp

#include <stdio.h>
#include "string"
using namespace std;


class ComponentType{
public:
    ComponentType(int val) : value(val){}
   
    static const int arm = 0;
    static const int torso = 1;
    static const int head = 2;
    static const int arms = 3;
    static const int locomotor = 4;
    string to_string(){
        switch(value) {
            case(arm):return "arm";
            case(torso):return "torso";
            case(head):return "head";
            case(locomotor):return "locomotor";
            default: return "UNKNOWN";
        }
        
    }
    
private:
    int value;
    
    


};
#endif /* ComponentType_hpp */
